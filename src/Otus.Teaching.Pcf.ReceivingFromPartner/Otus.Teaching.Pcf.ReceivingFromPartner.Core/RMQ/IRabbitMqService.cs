﻿using System;

using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core;
public interface IRabbitMqService
{
    void SendPromoCodeMessage(PromoCode message);
    void SendEmployeeMessage(Guid message);
}


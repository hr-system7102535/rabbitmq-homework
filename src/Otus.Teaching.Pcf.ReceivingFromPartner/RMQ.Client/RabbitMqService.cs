﻿using System.Text;

using Newtonsoft.Json;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core;
using RabbitMQ.Client;
using RMQ.DTO;

namespace RMQ.Client;
public class RabbitMqService : IRabbitMqService
{
    private readonly IModel channel;
    public RabbitMqService()
    {
        ConnectionFactory factory = new ConnectionFactory
        {
            HostName = "localhost",
            UserName = "rmuser",
            Password = "rmpassword",
            VirtualHost = "/",
        };

        var connection = factory.CreateConnection();
        this.channel = connection.CreateModel();

        this.channel.QueueDeclare(queue: "PromoCode",
                       durable: false,
                       exclusive: false,
                       autoDelete: false,
                       arguments: null);
        this.channel.QueueDeclare(queue: "Employee",
                       durable: false,
                       exclusive: false,
                       autoDelete: false,
                       arguments: null);
    }

    public GivePromoCodeToCustomerDto ConvertToMessage(PromoCode promCode) =>
        new GivePromoCodeToCustomerDto(
            promCode.ServiceInfo,
            promCode.PartnerId,
            promCode.Id,
            promCode.Code,
            promCode.PreferenceId,
            promCode.BeginDate.ToShortDateString(),
            promCode.EndDate.ToShortDateString(),
            promCode.PartnerManagerId);

    public void SendPromoCodeMessage(PromoCode promCode)
    {
        string message = JsonConvert.SerializeObject(this.ConvertToMessage(promCode));
        var body = Encoding.UTF8.GetBytes(message);
        channel.BasicPublish(
            exchange: "",
            routingKey: "PromoCode",
            basicProperties: null,
            body: body);
    }

    public void SendEmployeeMessage(Guid employeeID)
    {
        string message = JsonConvert.SerializeObject(employeeID);
        var body = Encoding.UTF8.GetBytes(message);
        channel.BasicPublish(
            exchange: "",
            routingKey: "Employee",
            basicProperties: null,
            body: body);
    }
}


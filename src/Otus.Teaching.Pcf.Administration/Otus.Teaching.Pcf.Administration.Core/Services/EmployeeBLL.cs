﻿using System;
using System.Threading.Tasks;

using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.Core.Services
{
    public class EmployeeBLL: IEmployeeBLL
    {
        private readonly IRepository<Employee> employeeRepository;
        public EmployeeBLL(IRepository<Employee> employeeRepository)
        {
            this.employeeRepository = employeeRepository;
        }

        public async Task<bool> UpdateEmployeePromocodeCount(Guid employeeID)
        {
            var employee = await this.employeeRepository.GetByIdAsync(employeeID);

            if (employee == null)
            {
                return false;
            }
            
            employee.AppliedPromocodesCount++;

            await this.employeeRepository.UpdateAsync(employee);

            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions.Services
{
    public interface IEmployeeBLL
    {
        Task<bool> UpdateEmployeePromocodeCount(Guid employeeID);
    }
}

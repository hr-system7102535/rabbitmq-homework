﻿using System.Text;

using Microsoft.Extensions.Hosting;

using Newtonsoft.Json;

using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;

using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RMQ.Client
{
    public class RabbitMQService : BackgroundService
    {
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly IEmployeeBLL _employeeBLL;
        public RabbitMQService(IEmployeeBLL employeeBLL)
        {
            _employeeBLL = employeeBLL;
            ConnectionFactory factory = new ConnectionFactory
            {
                HostName = "localhost",
                UserName = "rmuser",
                Password = "rmpassword",
                VirtualHost = "/",
            };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(
                queue: "Employee",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (ch, ea) =>
            {
                var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                var obj = JsonConvert.DeserializeObject<Guid>(content);

                _employeeBLL.UpdateEmployeePromocodeCount(obj);

                _channel.BasicAck(ea.DeliveryTag, false);
            };

            _channel.BasicConsume("Employee", false, consumer);

            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _channel.Close();
            _connection.Close();
            base.Dispose();
        }
    }
}
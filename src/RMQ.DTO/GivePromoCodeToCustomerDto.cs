﻿
namespace RMQ.DTO;

public record GivePromoCodeToCustomerDto(
string ServiceInfo,
Guid PartnerId,
Guid PromoCodeId,
string PromoCode,
Guid PreferenceId,
string BeginDate,
string EndDate,
Guid? PartnerManagerId);

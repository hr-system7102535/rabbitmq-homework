﻿using System.Text;

using Microsoft.Extensions.Hosting;

using Newtonsoft.Json;

using Otus.GivingToCustomer.BLL;

using RabbitMQ.Client;
using RabbitMQ.Client.Events;

using RMQ.DTO;

namespace RMQ.Client
{
    public class RabbitMQService : BackgroundService
    {
        private IConnection _connection;
        private IModel _channel;
        private IPromocodesBLL _promocodesBLL;
        public RabbitMQService(IPromocodesBLL promocodesBLL)
        {
            _promocodesBLL = promocodesBLL;
            ConnectionFactory factory = new ConnectionFactory
            {
                HostName = "localhost",
                UserName = "rmuser",
                Password = "rmpassword",
                VirtualHost = "/",
            };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(
                queue: "PromoCode",
                durable: false, 
                exclusive: false, 
                autoDelete: false, 
                arguments: null);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (ch, ea) =>
            {
                var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                var obj = JsonConvert.DeserializeObject<GivePromoCodeToCustomerDto>(content);

                _promocodesBLL.GivePromoCodesToCustomersWithPreference(obj);

                _channel.BasicAck(ea.DeliveryTag, false);
            };

            _channel.BasicConsume("PromoCode", false, consumer);

            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _channel.Close();
            _connection.Close();
            base.Dispose();
        }
    }
}
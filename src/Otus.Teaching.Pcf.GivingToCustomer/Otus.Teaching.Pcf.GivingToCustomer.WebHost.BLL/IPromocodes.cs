﻿namespace Otus.Teaching.Pcf.GivingToCustomer.BLL
{
    public interface IPromocodesBLL
    {
        void GivePromoCodesToCustomersWithPreference(GivePromoCodeToCustomerDto obj);
    }
}

﻿using Otus.Teaching.Pcf.GivingToCustomer.Core;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;

namespace Otus.Teaching.Pcf.GivingToCustomer.BLL;

public class PromocodesBLL : IPromocodesBLL
{
    private readonly IRepository<PromoCode> _promoCodesRepository;
    private readonly IRepository<Customer> _customersRepository;
    private readonly IRepository<Preference> _preferencesRepository;
    public PromocodesBLL(
        IRepository<PromoCode> promoCodesRepository,
        IRepository<Preference> preferencesRepository,
        IRepository<Customer> customersRepository)
    {
        _customersRepository = customersRepository;
        _promoCodesRepository = promoCodesRepository;
        _preferencesRepository = preferencesRepository;
    }

    public void GivePromoCodesToCustomersWithPreference(GivePromoCodeToCustomerDto obj)
    {
        var preference = _preferencesRepository.GetByIdAsync(obj.PreferenceId).Result;
        var customers = _customersRepository
         .GetWhere(d => d.Preferences.Any(x =>
             x.Preference.Id == preference.Id)).Result;

        PromoCode promoCode = PromoCodeMapper.MapFromModel(obj, preference, customers);

        _promoCodesRepository.AddAsync(promoCode);
    }
}

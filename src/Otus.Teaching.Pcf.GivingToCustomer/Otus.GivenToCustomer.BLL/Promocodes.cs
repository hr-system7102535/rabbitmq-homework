﻿using Otus.GivenToCustomer.BLL;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

using RMQ.DTO;

namespace Otus.GivingToCustomer.BLL;

public class PromocodesBLL : IPromocodesBLL
{
    private readonly IRepository<PromoCode> _promoCodesRepository;
    private readonly IRepository<Customer> _customersRepository;
    private readonly IRepository<Preference> _preferencesRepository;
    public PromocodesBLL(
        IRepository<PromoCode> promoCodesRepository,
        IRepository<Preference> preferencesRepository,
        IRepository<Customer> customersRepository)
    {
        _customersRepository = customersRepository;
        _promoCodesRepository = promoCodesRepository;
        _preferencesRepository = preferencesRepository;
    }

    public bool GivePromoCodesToCustomersWithPreference(GivePromoCodeToCustomerDto obj)
    {
        var preference = _preferencesRepository.GetByIdAsync(obj.PreferenceId).Result;
        if(preference != null)
        {
            return false;
        }

        var customers = _customersRepository
         .GetWhere(d => d.Preferences.Any(x =>
             x.Preference.Id == preference.Id)).Result;

        PromoCode promoCode = PromoCodeMapperBLL.MapFromDTO(obj, preference, customers);

        _promoCodesRepository.AddAsync(promoCode);

        return true;
    }
}

﻿using RMQ.DTO;

namespace Otus.GivingToCustomer.BLL
{
    public interface IPromocodesBLL
    {
        bool GivePromoCodesToCustomersWithPreference(GivePromoCodeToCustomerDto obj);
    }
}
